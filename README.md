# FastManta #

### Authors ###

* Thomas 'Alien' Guerville - thomas.guerville@epita.fr

### Purpose ###

* Fastmanta is a ray tracer

### Needed packages ###

* Boost program_options

### Building ###

* cd fastmanta/
* ./configure
* make

### Usage ###

* ./fastmanta -i *input-file* [options]

### Options ###

* --verbose: makes FastManta more talkative on its behavior
* --output | -o *output-file*: specifies the output file's name (default: output.ppm)
* --anti-aliasing | -a: specifies the anti-aliasing coefficient (default: 1)
