#pragma once

# include <cassert>

# include "color.hh"

namespace misc
{
  inline
  Color::Color()
  : r_(0.0)
  , g_(0.0)
  , b_(0.0)
  {}

  inline
  Color::Color(const Color& color)
  : r_(color.r_get())
  , g_(color.g_get())
  , b_(color.b_get())
  {}

  inline
  Color::Color(double r, double g, double b)
  : r_(r)
  , g_(g)
  , b_(b)
  {}

  inline
  Color::~Color()
  {}

  inline
  void
  Color::normalize()
  {
    r_ = r_ < 0.0 ? 0.0 : (r_ > 255.0 ? 255.0 : r_);
    g_ = g_ < 0.0 ? 0.0 : (g_ > 255.0 ? 255.0 : g_);
    b_ = b_ < 0.0 ? 0.0 : (b_ > 255.0 ? 255.0 : b_);
  }

  inline
  double
  Color::r_get() const
  {
    return r_;
  }

  inline
  double
  Color::g_get() const
  {
    return g_;
  }

  inline
  double
  Color::b_get() const
  {
    return b_;
  }

  inline
  Color
  operator+(const Color& c1, const Color& c2)
  {
    return Color(c1.r_get() + c2.r_get(),
                 c1.g_get() + c2.g_get(),
                 c1.b_get() + c2.b_get());
  }

  inline
  Color
  operator*(const Color& c1, const Color& c2)
  {
    return Color(c1.r_get() * c2.r_get(),
                 c1.g_get() * c2.g_get(),
                 c1.b_get() * c2.b_get());
  }

  inline
  Color
  operator/(const Color& c, double d)
  {
    assert(d);
    return Color(c.r_get() / d, c.g_get() / d, c.b_get() / d);
  }

  inline
  Color
  operator/(double d, const Color& c)
  {
    assert(d);
    return Color(c.r_get() / d, c.g_get() / d, c.b_get() / d);
  }

  inline
  Color
  operator*(const Color& c, double d)
  {
    return Color(c.r_get() * d, c.g_get() * d, c.b_get() * d);
  }

  inline
  Color
  operator*(double d, const Color& c)
  {
    return Color(c.r_get() * d, c.g_get() * d, c.b_get() * d);
  }

} // misc
