#pragma once

# include "ray.hh"
# include "vector.hh"

namespace misc
{
  inline
  Ray::Ray(const Vector3& origin, const Vector3& dir)
  : origin_(origin)
  , dir_(dir)
  {}

  inline
  Ray::~Ray()
  {}

  inline
  const Vector3&
  Ray::origin_get() const
  {
    return origin_;
  }

  inline
  const Vector3&
  Ray::dir_get() const
  {
    return dir_;
  }

} // misc
