#pragma once

# include <array>
# include <cassert>
# include <cmath>
# include <cfloat>

# include "polynomial.hh"
# include "values.hh"

namespace misc
{
  template <unsigned int Degree>
  inline
  Polynomial<Degree>::Polynomial(std::array<double, Degree + 1>& coefs)
  : coefs_(coefs)
  {}

  template <unsigned int Degree>
  inline
  Polynomial<Degree>::~Polynomial()
  {}

  template <unsigned int Degree>
  inline
  unsigned int
  Polynomial<Degree>::resolve(std::array<double, Degree>& squares) const
  {
    /* Unreachable */
    assert(false);
  }

  template <>
  inline
  unsigned int
  Polynomial<2>::resolve(std::array<double, 2>& squares) const
  {
    double delta = coefs_[1] * coefs_[1] - 4.0 * coefs_[0] * coefs_[2];

    if (!delta)
    {
      squares[0] = (-coefs_[1]) / (2.0 * coefs_[0]);
      return 1;
    }

    else if (delta > 0)
    {
      squares[0] = (-coefs_[1] - std::sqrt(delta)) / (2.0 * coefs_[0]);
      squares[1] = (-coefs_[1] + std::sqrt(delta)) / (2.0 * coefs_[0]);
      return 2;
    }

    return 0;
  }

  template <>
  inline
  unsigned int
  Polynomial<3>::resolve(std::array<double, 3>& squares) const
  {
    double b = coefs_[1] / coefs_[0];
    double c = coefs_[2] / coefs_[0];
    double d = coefs_[3] / coefs_[0];

    double q = (b * b - 3.0 * c) / 9.0;
    double r = (b * (b * b - 4.5 * c) + 13.5 * d) / 27.0;
    double delta = q * q * q - r * r;

    if (delta >= 0.0)
    {
      double delta_prime = r / std::exp((3.0 / 2.0) * std::log(q));
      double t = std::acos(delta_prime) / 3.0;
      double s = -2.0 * std::sqrt(q);

      for (int i = 0; i < 3; ++i)
        squares[i] = s * std::cos(t + 2.0 * values::PI_ * (i / 3.0)) - b / 3.0;

      return 3;
    }
    else
    {
      double s = std::exp((1.0 / 3.0) * std::log(std::sqrt(-delta)
                 + std::abs(r)));

      if (r < 0.0)
        squares[0] = s + q / s - b / 3.0;
      else
        squares[0] = -s - q / s - b / 3.0;

      return 1;
    }
  }

  template <>
  inline
  unsigned int
  Polynomial<4>::resolve(std::array<double, 4>& squares) const
  {
    if (coefs_[0] == 0.0 && coefs_[1] == 0.0)
    {
      std::array<double, 3> coefs = { coefs_[2], coefs_[3], coefs_[4] };
      std::array<double, 2> new_squares = { 0.0, 0.0 };
      auto poly = Polynomial<2>(coefs);
      auto nb_solutions = poly.resolve(new_squares);

      for (unsigned int i = 0; i < nb_solutions; ++i)
        squares[i] = new_squares[i];

      return nb_solutions;
    }

    if (coefs_[0] == 0.0)
    {
      std::array<double, 4> coefs = { coefs_[1], coefs_[2], coefs_[3], coefs_[4] };
      std::array<double, 3> new_squares = { 0.0, 0.0, 0.0 };
      auto poly = Polynomial<3>(coefs);
      auto nb_solutions = poly.resolve(new_squares);

      for (unsigned int i = 0; i < nb_solutions; ++i)
        squares[i] = new_squares[i];

      return nb_solutions;
    }

    double b = coefs_[1] / coefs_[0];
    double c = coefs_[2] / coefs_[0];
    double d = coefs_[3] / coefs_[0];
    double e = coefs_[4] / coefs_[0];

    if (e == 0.0)
    {
      std::array<double, 4> coefs = { 1, b, c, d };
      std::array<double, 3> new_squares = { 0.0, 0.0, 0.0 };
      auto poly = Polynomial<3>(coefs);
      auto nb_solutions = poly.resolve(new_squares);

      for (unsigned int i = 0; i < nb_solutions; ++i)
        squares[i + 1] = new_squares[i];

      return nb_solutions + 1;
    }

    double p = -0.375 * b * b + c;
    double q = 0.125 * b * b * b - 0.5 * b * c + d;
    double r = -0.01171875 * b * b * b * b + 0.0625 * b * b * c - 0.25 * b * d + e;

    std::array<double, 4> coefs = { 1, -0.5 * p, -r, 0.5 * r * p - 0.125 * q * q };
    std::array<double, 3> new_squares = { 0.0, 0.0, 0.0 };
    auto poly = Polynomial<3>(coefs);
    auto nb_solutions = poly.resolve(new_squares);

    double z0 = DBL_MAX;
    if (nb_solutions == 1)
      z0 = new_squares[0];
    else
      for (unsigned int i = 0; i < nb_solutions; ++i)
        if (new_squares[i] < z0 && new_squares[i] >= 0.0)
          z0 = new_squares[i];

    if (z0 == DBL_MAX)
      return 0;

    double d1 = 2.0 * z0 - p;
    double d2;

    if (d1 < 0.0)
    {
      if (d1 > -values::EPSILON_)
        d1 = 0.0;
      else
        return 0;
    }

    if (d1 < values::EPSILON_)
    {
      d2 = z0 * z0 - r;

      if (d2 < 0.0)
        return 0;
      else
        d2 = std::sqrt(d2);
    }
    else
    {
      d1 = std::sqrt(d1);
      d2 = 0.5 * q / d1;
    }

    double index = 0;
    p = d1 * d1 - 4.0 * (z0 - d2);

    if (p == 0.0)
      squares[index++] = -0.5 * d1 + 0.25 * b;
    else if (p > 0.0)
    {
       p = std::sqrt(p);
       squares[index++] = -0.5 * (d1 + p) - 0.25 * b;
       squares[index++] = -0.5 * (d1 - p) - 0.25 * b;
    }

    p = d1 * d1 - 4.0 * (z0 + d2);

    if (p == 0.0)
      squares[index++] = 0.5 * d1 + 0.25 * b;
    else if (p > 0.0)
    {
       p = std::sqrt(p);
       squares[index++] = 0.5 * (d1 + p) - 0.25 * b;
       squares[index++] = 0.5 * (d1 - p) - 0.25 * b;
    }

    return index;
  }

} // misc
