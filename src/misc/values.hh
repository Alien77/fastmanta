#pragma once

namespace values
{
  constexpr int SUCCES_ =          0;
  constexpr int CMD_ERROR_ =       1;

  constexpr double PI_ =           3.14159265359;
  constexpr double PI_4_ =         0.7853981633;
  constexpr double EPSILON_ =      0.0000001;

  constexpr double AMBIENT_COEF_ = 0.300;
  constexpr double SHADOW_COEF_ =  0.300;

} // values
