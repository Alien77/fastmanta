#pragma once

# include <array>

namespace misc
{
  template <unsigned int Degree>
  class Polynomial final
  {
    public:
      Polynomial(std::array<double, Degree + 1>& coefs);
      ~Polynomial();

      unsigned int resolve(std::array<double, Degree>& squares) const;

    private:
      std::array<double, Degree + 1> coefs_;
  };

} // misc

# include "polynomial.hxx"
