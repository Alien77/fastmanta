#pragma once

# include "vector.hh"
# include "scene/screen.hh"
# include "scene/camera.hh"

namespace misc
{
  class Ray final
  {
    public:
      Ray(const Vector3& origin, const Vector3& dir);
      ~Ray();

      const Vector3& origin_get() const;
      const Vector3& dir_get() const;

      static const Ray get_ray_at(double x, double y,
                                  const scene::Camera* camera,
                                  const scene::Screen* screen);

    private:
      const Vector3 origin_;
      const Vector3 dir_;
  };

} // misc

# include "ray.hxx"
