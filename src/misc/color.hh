#pragma once

namespace misc
{
  class Color final
  {
    public:
      Color();
      Color(const Color& color);
      Color(double r, double g, double b);
      ~Color();

      void normalize();

      double r_get() const;
      double g_get() const;
      double b_get() const;

    private:
      double r_;
      double g_;
      double b_;
  };

  Color operator+(const Color& c1, const Color& c2);
  Color operator*(const Color& c1, const Color& c2);
  Color operator/(const Color& c, double d);
  Color operator/(double d, const Color& c);
  Color operator*(const Color& c, double d);
  Color operator*(double d, const Color& c);

} // misc

# include "color.hxx"
