#pragma once

namespace misc
{
  class Vector3 final
  {
    public:
      Vector3();
      Vector3(const Vector3& vector);
      Vector3(double x, double y, double z);
      ~Vector3();

      double norm_get() const;
      void normalize();

      static double distance_between(const Vector3& u, const Vector3& v);

      double x_get() const;
      double y_get() const;
      double z_get() const;

    private:
      double x_;
      double y_;
      double z_;
  };

  Vector3 operator+(const Vector3& u, const Vector3& v);
  Vector3 operator-(const Vector3& u, const Vector3& v);
  Vector3 operator-(const Vector3& u, const Vector3& v);
  double operator|(const Vector3& u, const Vector3& v);
  Vector3 operator*(const Vector3& u, double d);
  Vector3 operator*(double d, const Vector3& u);
  Vector3 operator/(const Vector3& u, double d);
  Vector3 operator/(double d, const Vector3& u);

} // misc

# include "vector.hxx"
