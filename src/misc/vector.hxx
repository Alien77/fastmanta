#pragma once

# include <cmath>

# include "vector.hh"

namespace misc
{
  inline
  Vector3::Vector3()
  : x_(0.0)
  , y_(0.0)
  , z_(0.0)
  {}

  inline
  Vector3::Vector3(const Vector3& vector)
  : x_(vector.x_get())
  , y_(vector.y_get())
  , z_(vector.z_get())
  {}

  inline
  Vector3::Vector3(double x, double y, double z)
  : x_(x)
  , y_(y)
  , z_(z)
  {}

  inline
  Vector3::~Vector3()
  {}

  inline
  double
  Vector3::norm_get() const
  {
    return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);
  }

  inline
  void
  Vector3::normalize()
  {
    if (auto norm = norm_get())
    {
      x_ /= norm;
      y_ /= norm;
      z_ /= norm;
    }
  }

  inline
  double
  Vector3::distance_between(const Vector3& u, const Vector3& v)
  {
    return std::sqrt((u.x_get() - v.x_get()) * (u.x_get() - v.x_get())
                   + (u.y_get() - v.y_get()) * (u.y_get() - v.y_get())
                   + (u.z_get() - v.z_get()) * (u.z_get() - v.z_get()));
  }

  inline
  double
  Vector3::x_get() const
  {
    return x_;
  }

  inline
  double
  Vector3::y_get() const
  {
    return y_;
  }

  inline
  double
  Vector3::z_get() const
  {
    return z_;
  }

  inline
  Vector3
  operator+(const Vector3& u, const Vector3& v)
  {
    return Vector3(u.x_get() + v.x_get(),
                   u.y_get() + v.y_get(),
                   u.z_get() + v.z_get());
  }

  inline
  Vector3
  operator-(const Vector3& u, const Vector3& v)
  {
    return Vector3(u.x_get() - v.x_get(),
                   u.y_get() - v.y_get(),
                   u.z_get() - v.z_get());
  }

  inline
  Vector3
  operator*(const Vector3& u, const Vector3& v)
  {
    return Vector3(u.y_get() * v.z_get() - u.z_get() * v.y_get(),
                   u.z_get() * v.x_get() - u.x_get() * v.z_get(),
                   u.x_get() * v.y_get() - u.y_get() * v.x_get());
  }

  inline
  double
  operator|(const Vector3& u, const Vector3& v)
  {
    return u.x_get() * v.x_get()
         + u.y_get() * v.y_get()
         + u.z_get() * v.z_get();
  }

  inline
  Vector3
  operator*(const Vector3& u, double d)
  {
    return Vector3(u.x_get() * d, u.y_get() * d, u.z_get() * d);
  }

  inline
  Vector3
  operator*(double d, const Vector3& u)
  {
    return Vector3(u.x_get() * d, u.y_get() * d, u.z_get() * d);
  }

  inline
  Vector3
  operator/(const Vector3& u, double d)
  {
    return Vector3(u.x_get() / d, u.y_get() / d, u.z_get() / d);
  }

  inline
  Vector3
  operator/(double d, const Vector3& u)
  {
    return Vector3(u.x_get() / d, u.y_get() / d, u.z_get() / d);
  }

} // misc
