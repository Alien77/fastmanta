#include "ray.hh"

namespace misc
{
  const Ray
  Ray::get_ray_at(double x, double y, const scene::Camera* camera,
                  const scene::Screen* screen)
  {
    double dx = x - static_cast<double>(screen->width_get()) / 2.0;
    double dy = y - static_cast<double>(screen->height_get()) / 2.0;

    auto u = camera->u_get() * dx;
    auto v = camera->v_get() * dy;

    auto point = screen->center_get() + u + v;
    auto dir = point - camera->origin_get();
    dir.normalize();

    return Ray(camera->origin_get(), dir);
  }

} // misc
