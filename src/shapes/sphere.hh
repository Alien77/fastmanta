#pragma once

# include "shape.hh"

namespace shapes
{
  class Sphere: public Shape
  {
    public:
      Sphere(const Material& mat, const misc::Vector3& center, double radius);
      ~Sphere();

      const misc::Vector3& center_get() const;
      double radius_get() const;

      double distance_from(const misc::Ray& ray) const override;
      misc::Vector3 normal_at(const misc::Vector3& point) const override;

    private:
      const misc::Vector3 center_;
      const double radius_;
  };

} // shapes

# include "sphere.hxx"
