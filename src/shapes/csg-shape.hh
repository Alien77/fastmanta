#pragma once

# include <string>

# include "shape.hh"

namespace shapes
{
  class CSGShape: public Shape
  {
    public:
      CSGShape(const Material& mat, const Shape* left, const Shape* right,
               const std::string& op);
      ~CSGShape();

      const Shape* left_get() const;
      const Shape* right_get() const;
      const std::string& op_get() const;

      double distance_from(const misc::Ray& ray) const override;
      misc::Vector3 normal_at(const misc::Vector3& point) const override;

    private:
      mutable const Shape* inter_shape_ = nullptr;
      const Shape* left_;
      const Shape* right_;
      const std::string op_;
  };

} // shapes

# include "csg-shape.hxx"
