#include <cfloat>
#include <cmath>
#include <cassert>

#include "csg-shape.hh"

namespace shapes
{
  double
  CSGShape::distance_from(const misc::Ray& ray) const
  {
    double t1 = left_->distance_from(ray);
    double t2 = right_->distance_from(ray);

    if (op_ == "DIFF")
    {
      if (t1 == DBL_MAX && t2 == DBL_MAX)
      {
        inter_shape_ = nullptr;
        return DBL_MAX;
      }
      else if (t1 == DBL_MAX && t2 != DBL_MAX)
      {
        inter_shape_ = nullptr;
        return DBL_MAX;
      }
      else if (t1 != DBL_MAX && t2 == DBL_MAX)
      {
        inter_shape_ = left_;
        return t1;
      }
      else if (t1 > t2)
      {
        inter_shape_ = right_;
        return t2;
      }
      else
      {
        inter_shape_ = left_;
        return t1;
      }
    }
    else if (op_ == "INTER")
    {
      if (t1 == DBL_MAX || t2 == DBL_MAX)
      {
        inter_shape_ = nullptr;
        return DBL_MAX;
      }
      else
      {
        if (t1 <= t2)
        {
          inter_shape_ = left_;
          return t1;
        }
        else
        {
          inter_shape_ = right_;
          return t2;
        }
      }
    }
    else if (op_ == "UNION")
    {
      if (t1 <= t2)
      {
        inter_shape_ = left_;
        return t1;
      }
      else
      {
        inter_shape_ = right_;
        return t2;
      }
    }
    else
    {
      // Unreachable
      assert(false);
    }
  }

  misc::Vector3
  CSGShape::normal_at(const misc::Vector3& point) const
  {
    if (op_ == "DIFF")
    {
      if (inter_shape_ == left_)
        return left_->normal_at(point);
      else
        return -1.0 * right_->normal_at(point);
    }
    else
    {
      if (inter_shape_ == left_)
        return left_->normal_at(point);
      else
        return right_->normal_at(point);
    }
  }

} // shapes
