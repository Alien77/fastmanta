#pragma once

# include "plane.hh"

namespace shapes
{
  inline
  Plane::Plane(const shapes::Material& mat, const misc::Vector3& n, double d)
  : Shape(mat)
  , normal_(n)
  , d_(d)
  {}

  inline
  Plane::~Plane()
  {}

  inline
  const misc::Vector3&
  Plane::normal_get() const
  {
    return normal_;
  }

  inline
  double
  Plane::d_get() const
  {
    return d_;
  }

} // shapes
