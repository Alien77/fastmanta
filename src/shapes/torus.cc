#include <cmath>
#include <cfloat>

#include "torus.hh"
#include "misc/polynomial.hh"

namespace shapes
{
  double
  Torus::distance_from(const misc::Ray& ray) const
  {
    // For shortest code lines
    double d0 = ray.dir_get().x_get();
    double d1 = ray.dir_get().y_get();
    double d2 = ray.dir_get().z_get();
    double p0 = ray.origin_get().x_get() - center_.x_get();
    double p1 = ray.origin_get().y_get() - center_.y_get();
    double p2 = ray.origin_get().z_get() - center_.z_get();
    double R = big_radius_;
    double r = little_radius_;

    double a = std::pow(d0 * d0 + d1 * d1 + d2 * d2, 2.0);
    double b = 4.0 * (d0 * d0 + d1 * d1 + d2 * d2) * (d0 * p0 + d1 * p1 + d2 * p2);
    double c = 2.0 * (d0 * d0 + d1 * d1 + d2 * d2) * (p0 * p0 + p1 * p1 + p2 * p2
               + R * R - r * r) + 4.0 * std::pow(d0 * p0 + d1 * p1 + d2 * p2, 2)
               - 4.0 * R * R * (d0 * d0 + d2 * d2);
    double d = 4.0 * (d0 * p0 + d1 * p1 + d2 * p2) * (p0 * p0 + p1 * p1 + p2 * p2
               + R * R - r * r) - 8.0 * R * R * (d0 * p0 + d2 * p2);
    double e = std::pow(p0 * p0 + p1 * p1 + p2 * p2 + R * R - r * r, 2) - 4.0
               * R * R * (p0 * p0 + p2 * p2);

    std::array<double, 5> coefs = { a, b, c, d, e };
    std::array<double, 4> squares = { 0.0, 0.0, 0.0, 0.0 };
    auto poly = misc::Polynomial<4>(coefs);
    auto nb_solutions = poly.resolve(squares);

    double sol = DBL_MAX;
    for (unsigned int i = 0; i < nb_solutions; ++i)
      if (squares[i] < sol && squares[i] >= 0.0)
        sol = squares[i];

    return sol;
  }

  misc::Vector3
  Torus::normal_at(const misc::Vector3& point) const
  {
    double x = point.x_get() - center_.x_get();
    double y = point.y_get() - center_.y_get();
    double z = point.z_get() - center_.z_get();
    double R = big_radius_;
    double r = little_radius_;

    double dx = 4.0 * x * (x * x + y * y + z * z + R * R - r * r)
                - 8.0 * R * R * x;
    double dy = 4.0 * y * (x * x + y * y + z * z + R * R - r * r);
    double dz = 4.0 * z * (x * x + y * y + z * z + R * R - r * r)
                - 8.0 * R * R * z;

    auto normal = misc::Vector3(dx, dy, dz);
    normal.normalize();

    return normal;
  }

} // shapes
