#pragma once

# include "torus.hh"

namespace shapes
{
  inline
  Torus::Torus(const Material& mat, const misc::Vector3& center,
               double little_radius, double big_radius)
  : Shape(mat)
  , center_(center)
  , little_radius_(little_radius)
  , big_radius_(big_radius)
  {}

  inline
  Torus::~Torus()
  {}

  inline
  const misc::Vector3&
  Torus::center_get() const
  {
    return center_;
  }

  inline
  double
  Torus::little_radius_get() const
  {
    return little_radius_;
  }

  inline
  double
  Torus::big_radius_get() const
  {
    return big_radius_;
  }

} // shapes
