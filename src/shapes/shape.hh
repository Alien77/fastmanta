#pragma once

# include "material.hh"
# include "misc/vector.hh"
# include "misc/ray.hh"

namespace shapes
{
  class Shape
  {
    public:
      Shape(const Material& material);
      virtual ~Shape();

      const Material& material_get() const;

      virtual double distance_from(const misc::Ray& ray) const = 0;
      virtual misc::Vector3 normal_at(const misc::Vector3& point) const = 0;

    protected:
      const Material material_;
  };

} // shapes

# include "shape.hxx"
