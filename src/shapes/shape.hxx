#pragma once

# include "shape.hh"

namespace shapes
{
  inline
  Shape::Shape(const Material& material)
  : material_(material)
  {}

  inline
  Shape::~Shape()
  {}

  inline
  const Material&
  Shape::material_get() const
  {
    return material_;
  }

} // shapes
