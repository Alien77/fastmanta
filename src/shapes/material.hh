#pragma once

# include "misc/color.hh"

namespace shapes
{
  class Material final
  {
    public:
      Material(const misc::Color& color, double diff, double spec,
               double shin, double refl);
      ~Material();

      const misc::Color& color_get() const;
      double diff_get() const;
      double spec_get() const;
      double shin_get() const;
      double refl_get() const;

    private:
      const misc::Color color_;
      const double diff_;
      const double spec_;
      const double shin_;
      const double refl_;
  };

} // shapes

# include "material.hxx"
