#include <array>
#include <cfloat>

#include "sphere.hh"
#include "misc/polynomial.hh"
#include "misc/vector.hh"

namespace shapes
{
  double
  Sphere::distance_from(const misc::Ray& ray) const
  {
    double a = ray.dir_get() | ray.dir_get();
    double b = 2.0 * (ray.dir_get() | (ray.origin_get() - center_));
    double c = ((ray.origin_get() - center_) | (ray.origin_get() - center_))
               - radius_ * radius_;

    std::array<double, 3> coefs = { a, b, c };
    std::array<double, 2> squares = { 0.0, 0.0 };
    auto poly = misc::Polynomial<2>(coefs);

    auto nb_solutions = poly.resolve(squares);

    if (nb_solutions == 1)
    {
      if (squares[0] < 0.0)
        return DBL_MAX;

      return squares[0];
    }
    else if (nb_solutions == 2)
    {
      if (squares[0] < 0.0 && squares[1] < 0.0)
        return DBL_MAX;
      else if (squares[0] < 0.0)
        return squares[1];
      else if (squares[1] < 0.0)
        return squares[0];
      else
        return std::min(squares[0], squares[1]);
    }

    return DBL_MAX;
  }

  misc::Vector3
  Sphere::normal_at(const misc::Vector3& point) const
  {
    auto normal = point - center_;
    normal.normalize();
    return normal;
  }

} // shapes
