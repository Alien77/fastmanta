#pragma once

# include "shape.hh"

namespace shapes
{
  class Torus: public Shape
  {
    public:
      Torus(const Material& mat, const misc::Vector3& center,
            double little_radius, double big_radius);
      ~Torus();

      const misc::Vector3& center_get() const;
      double little_radius_get() const;
      double big_radius_get() const;

      double distance_from(const misc::Ray& ray) const override;
      misc::Vector3 normal_at(const misc::Vector3& point) const override;

    private:
      const misc::Vector3 center_;
      const double little_radius_;
      const double big_radius_;
  };

} // shapes

# include "torus.hxx"
