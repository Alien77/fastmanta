#pragma once

# include "shape.hh"

namespace shapes
{
  inline
  CSGShape::CSGShape(const Material& mat, const Shape* left,
                     const Shape* right, const std::string& op)
  : Shape(mat)
  , left_(left)
  , right_(right)
  , op_(op)
  {}

  inline
  CSGShape::~CSGShape()
  {}

  inline
  const Shape*
  CSGShape::left_get() const
  {
    return left_;
  }

  inline
  const Shape*
  CSGShape::right_get() const
  {
    return right_;
  }

  inline
  const std::string&
  CSGShape::op_get() const
  {
    return op_;
  }

} // shapes
