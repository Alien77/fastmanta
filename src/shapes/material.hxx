#pragma once

# include "material.hh"
# include "misc/color.hh"

namespace shapes
{
  inline
  Material::Material(const misc::Color& color, double diff, double spec,
                     double shin, double refl)
  : color_(color)
  , diff_(diff)
  , spec_(spec)
  , shin_(shin)
  , refl_(refl)
  {}

  inline
  Material::~Material()
  {}

  inline
  const misc::Color&
  Material::color_get() const
  {
    return color_;
  }

  inline
  double
  Material::diff_get() const
  {
    return diff_;
  }

  inline
  double
  Material::spec_get() const
  {
    return spec_;
  }

  inline
  double
  Material::shin_get() const
  {
    return shin_;
  }

  inline
  double
  Material::refl_get() const
  {
    return refl_;
  }

} // shapes
