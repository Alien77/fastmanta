#pragma once

# include "shape.hh"

namespace shapes
{
  class Plane: public Shape
  {
    public:
      Plane(const shapes::Material& mat, const misc::Vector3& norm, double d);
      ~Plane();

      const misc::Vector3& normal_get() const;
      double d_get() const;

      double distance_from(const misc::Ray& ray) const override;
      misc::Vector3 normal_at(const misc::Vector3& point) const override;

    private:
      const misc::Vector3 normal_;
      const double d_;
  };

} // shapes

# include "plane.hxx"
