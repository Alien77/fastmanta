#include <algorithm>
#include <cfloat>

#include "plane.hh"
#include "misc/values.hh"

namespace shapes
{
  double
  Plane::distance_from(const misc::Ray& ray) const
  {
    double num = (normal_ | ray.origin_get()) + d_;
    double den = normal_ | ray.dir_get();

    if (std::abs(den) < values::EPSILON_)
      return DBL_MAX;

    double t = -num / den;

    return t < 0.0 ? DBL_MAX : t;
  }

  misc::Vector3
  Plane::normal_at(const misc::Vector3&) const
  {
    return normal_;
  }

} // plane
