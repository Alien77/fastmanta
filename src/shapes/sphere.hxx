#pragma once

# include "sphere.hh"

namespace shapes
{
  inline
  Sphere::Sphere(const Material& m, const misc::Vector3& center, double radius)
  : Shape(m)
  , center_(center)
  , radius_(radius)
  {}

  inline
  Sphere::~Sphere()
  {}

  inline
  const misc::Vector3&
  Sphere::center_get() const
  {
    return center_;
  }

  inline
  double
  Sphere::radius_get() const
  {
    return radius_;
  }

} // shapes
