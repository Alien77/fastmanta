#pragma once

# include "misc/vector.hh"

namespace scene
{
  class Camera final
  {
    public:
      Camera(const misc::Vector3& origin,
             const misc::Vector3& u,
             const misc::Vector3& v);
      ~Camera();

      const misc::Vector3& origin_get() const;
      const misc::Vector3& u_get() const;
      const misc::Vector3& v_get() const;

    private:
      const misc::Vector3 origin_;
      const misc::Vector3 u_;
      const misc::Vector3 v_;
  };

} // scene

# include "camera.hxx"
