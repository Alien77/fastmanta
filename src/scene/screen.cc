#include <cmath>

#include "screen.hh"
#include "misc/vector.hh"
#include "misc/values.hh"

namespace scene
{
  void
  Screen::center_set(const Camera* camera)
  {
    auto u = misc::Vector3(camera->u_get());
    auto v = misc::Vector3(camera->v_get());

    u.normalize();
    v.normalize();

    auto w = u * v;
    double dist = (width_ * 2.0) / (std::tan(values::PI_4_ / 2.0));
    w = w * dist;

    center_ = camera->origin_get() + w;
  }

} // scene
