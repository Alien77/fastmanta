#pragma once

# include "misc/vector.hh"
# include "camera.hh"

namespace scene
{
  class Screen final
  {
    public:
      Screen(unsigned int width, unsigned int height);
      ~Screen();

      void center_set(const Camera* camera);

      unsigned int width_get() const;
      unsigned int height_get() const;
      const misc::Vector3& center_get() const;

    private:
      unsigned int width_;
      unsigned int height_;
      misc::Vector3 center_;
  };

} // scene

# include "screen.hxx"
