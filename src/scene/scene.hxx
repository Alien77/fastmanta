#pragma once

# include "scene.hh"

namespace scene
{
  inline
  Scene::Scene(const Screen* screen, const Camera* camera)
  : screen_(screen)
  , camera_(camera)
  {}

  inline
  Scene::~Scene()
  {
    for (const auto* shape : shapes_)
      delete shape;

    for (const auto* light : lights_)
      delete light;

    delete ambient_;
    delete camera_;
    delete screen_;
  }

  inline
  const Screen*
  Scene::screen_get() const
  {
    return screen_;
  }

  inline
  const Camera*
  Scene::camera_get() const
  {
    return camera_;
  }

  inline
  void
  Scene::add_shape(const shapes::Shape* shape)
  {
    shapes_.push_back(shape);
  }

  inline
  void
  Scene::add_light(const lights::Light* light)
  {
    lights_.push_back(light);
  }

  inline
  const std::vector<const shapes::Shape*>&
  Scene::shapes_get() const
  {
    return shapes_;
  }

  inline
  const std::vector<const lights::Light*>&
  Scene::lights_get() const
  {
    return lights_;
  }

  inline
  const lights::Ambient*
  Scene::ambient_get() const
  {
    return ambient_;
  }

  inline
  void
  Scene::ambient_set(const lights::Ambient* ambient)
  {
    ambient_ = ambient;
  }

} // scene
