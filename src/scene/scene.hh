#pragma once

# include <vector>
#include <lights/ambient.hh>

# include "screen.hh"
# include "camera.hh"
# include "shapes/shape.hh"
# include "lights/light.hh"

namespace scene
{
  class Scene final
  {
    public:
      Scene(const Screen* screen, const Camera* camera);
      ~Scene();

      const Screen* screen_get() const;
      const Camera* camera_get() const;

      void add_shape(const shapes::Shape* shape);
      void add_light(const lights::Light* light);

      const lights::Ambient* ambient_get() const;
      void ambient_set(const lights::Ambient* ambient);

      const std::vector<const shapes::Shape*>& shapes_get() const;
      const std::vector<const lights::Light*>& lights_get() const;

    private:
      const Screen* screen_;
      const Camera* camera_;

      std::vector<const shapes::Shape*> shapes_;
      std::vector<const lights::Light*> lights_;

      const lights::Ambient* ambient_;

  };

} // scene

# include "scene.hxx"
