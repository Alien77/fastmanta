#pragma once

# include "camera.hh"

namespace scene
{
  inline
  Camera::Camera(const misc::Vector3& origin,
                 const misc::Vector3& u,
                 const misc::Vector3& v)
  : origin_(origin)
  , u_(u)
  , v_(v)
  {}

  inline
  Camera::~Camera()
  {}

  inline
  const misc::Vector3&
  Camera::origin_get() const
  {
    return origin_;
  }

  inline
  const misc::Vector3&
  Camera::u_get() const
  {
    return u_;
  }

  inline
  const misc::Vector3&
  Camera::v_get() const
  {
    return v_;
  }

} // scene
