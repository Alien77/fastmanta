#pragma once

# include "screen.hh"

namespace scene
{
  inline
  Screen::Screen(unsigned int width, unsigned int height)
  : width_(width)
  , height_(height)
  {}

  inline
  Screen::~Screen()
  {}

  inline
  unsigned int
  Screen::width_get() const
  {
    return width_;
  }

  inline
  unsigned int
  Screen::height_get() const
  {
    return height_;
  }

  inline
  const misc::Vector3&
  Screen::center_get() const
  {
    return center_;
  }

} // scene
