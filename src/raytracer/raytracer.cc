#include <cfloat>
#include <iostream>
#include <fstream>

#include "raytracer.hh"

namespace raytracer
{
  void
  Raytracer::display_percentage(unsigned int i, unsigned int j,
                std::chrono::time_point<std::chrono::steady_clock>& time) const
  {
    auto* screen = scene_->screen_get();
    auto current_time = std::chrono::steady_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>
                   (current_time - time).count();

    if (elapsed >= 1000000.0)
    {
      int percentage = (float)(j * screen->width_get() + i)
                       / (float)(screen->width_get() * screen->height_get())
                       * 100;

      std::cout << "Processing ray tracing... "
                << percentage << "\%" << std::endl;
      time = current_time;
    }
  }

  void
  Raytracer::display_total_time(
               std::chrono::time_point<std::chrono::steady_clock>& begin) const
  {
    auto end = std::chrono::steady_clock::now();
    double duration = std::chrono::duration_cast<std::chrono::microseconds>
                       (end - begin).count() / 1000000.0;

    std::cout << "Ray tracing succeded in " << duration
              << " seconds" << std::endl;
  }

  void
  Raytracer::raytrace_scene(std::string output) const
  {
    std::ofstream file(output + ".ppm");
    auto* screen = scene_->screen_get();
    misc::Color color;

    // PPM header
    file << "P3" << std::endl
         << screen->width_get() << " " << screen->height_get() << std::endl
         << "255" << std::endl;

    auto time = std::chrono::steady_clock::now();
    auto begin = std::chrono::steady_clock::now();

    // Loop through the screen
    for (unsigned int j = 0; j < screen->height_get(); ++j)
    {
      for (unsigned int i = 0; i < screen->width_get(); ++i)
      {
        // Get the color of the current pixel and normalize it
        color = raytrace_pixel(i, j) * 255.0;
        color.normalize();

        file << static_cast<int>(color.r_get()) << " "
             << static_cast<int>(color.g_get()) << " "
             << static_cast<int>(color.b_get()) << " ";

        if (verbose_)
          display_percentage(i, j, time);
      }

      file << std::endl;
    }

    file.close();

    if (verbose_)
      display_total_time(begin);
 }

  misc::Color
  Raytracer::raytrace_pixel(unsigned int x, unsigned int y) const
  {
    // Final color of the pixel
    auto color = misc::Color();

    // Loop through the subpixels
    for (double i = 0.0; i < anti_aliasing_; ++i)
    {
      for (double j = 0.0; j < anti_aliasing_; ++j)
      {
        double di = i / anti_aliasing_;
        double dj = j / anti_aliasing_;

        // Get the ray pointing this subpixel
        auto ray = misc::Ray::get_ray_at(x + di, y + dj, scene_->camera_get(),
                                         scene_->screen_get());

        // Raytrace the current subpixel
        color = color + raytrace_subpixel(ray, true);
      }
    }

    // Compute the average color of the current pixel
    color = color / (anti_aliasing_ * anti_aliasing_);

    return color;
  }

  misc::Color
  Raytracer::raytrace_subpixel(const misc::Ray& ray, bool reflexion) const
  {
    auto color = misc::Color();
    double current = DBL_MAX;
    double min = DBL_MAX;
    const shapes::Shape* intersected_shape = nullptr;

    // Looking for the nearest intersected shape
    for (const auto* shape : scene_->shapes_get())
    {
      if ((current = shape->distance_from(ray)) < min)
      {
        min = current;
        intersected_shape = shape;
      }
    }

    // If no shape is intersected, return a black pixel
    if (!intersected_shape)
      return color;

    // Compute the intersection point
    auto intersection_point = ray.origin_get() + (min * ray.dir_get());

    // Get the normal at this point
    auto normal = intersected_shape->normal_at(intersection_point);

    // Compute the color of the pixel at this point
    for (const auto* light : scene_->lights_get())
    {
      color = color + light->apply_on(intersected_shape, normal,
                              intersection_point, scene_->ambient_get(), ray);

      compute_shadows(color, intersected_shape, intersection_point, light);

      if (reflexion)
        compute_reflexion(color, intersected_shape, intersection_point, normal, ray);
    }

    // Fake vectors will not be used anyway
    return color + scene_->ambient_get()->apply_on(intersected_shape,
           misc::Vector3(), misc::Vector3(), scene_->ambient_get(), ray);
  }

  void
  Raytracer::compute_shadows(misc::Color& color,
                             const shapes::Shape* intersected_shape,
                             const misc::Vector3& intersection_point,
                             const lights::Light* light) const
  {
    double current = DBL_MAX;

    if (auto* dir_ray = light->get_ray(intersection_point))
    {
      auto distance_from_light = light->distance_from(intersection_point);

      for (const auto* shape : scene_->shapes_get())
      {
        current = shape->distance_from(*dir_ray);

        if (current != DBL_MAX && shape != intersected_shape
            && current < distance_from_light)
        {
            color = color * values::SHADOW_COEF_;
            break;
        }
      }

      delete dir_ray;
    }
  }

  void
  Raytracer::compute_reflexion(misc::Color& color,
                               const shapes::Shape* shape,
                               const misc::Vector3& intersection_point,
                               const misc::Vector3& normal,
                               const misc::Ray& ray) const
  {
    // Leave function if the shape is not reflective
    if (shape->material_get().refl_get() == 0.0)
      return;

    auto dot = 2.0 * (ray.dir_get() | normal);
    auto reflect = (dot * normal) - ray.dir_get();
    reflect.normalize();

    auto new_ray = misc::Ray(intersection_point + 0.01 * reflect, reflect);
    auto reflected_color = raytrace_subpixel(new_ray, false);

    color = color + reflected_color * shape->material_get().refl_get();
  }

} // raytracer
