#pragma once

# include <string>
# include <chrono>

# include "scene/scene.hh"
# include "misc/color.hh"
# include "misc/ray.hh"

namespace raytracer
{
  class Raytracer final
  {
    public:
      Raytracer(const scene::Scene* scene);
      ~Raytracer();

      void raytrace_scene(std::string output) const;

      const scene::Scene* scene_get() const;
      unsigned int anti_aliasing_get() const;
      unsigned int threads_get() const;

      void anti_aliasing_set(unsigned int anti_aliasing);
      void threads_set(unsigned int threads);
      void verbose_set(bool verbose);

    private:
      void display_percentage(unsigned int i, unsigned int j,
               std::chrono::time_point<std::chrono::steady_clock>& time) const;
      void display_total_time(
              std::chrono::time_point<std::chrono::steady_clock>& begin) const;


      misc::Color raytrace_pixel(unsigned int x, unsigned int y) const;
      misc::Color raytrace_subpixel(const misc::Ray& ray, bool refl) const;

      void compute_shadows(misc::Color& color,
                           const shapes::Shape* intersected_shape,
                           const misc::Vector3& intersection_point,
                           const lights::Light* light) const;

      void compute_reflexion(misc::Color& color,
                             const shapes::Shape* intersected_shape,
                             const misc::Vector3& intersection_point,
                             const misc::Vector3& normal,
                             const misc::Ray& ray) const;

      const scene::Scene* scene_;
      unsigned int anti_aliasing_;
      unsigned int threads_;
      bool verbose_ = false;
  };

} //raytracer

# include "raytracer.hxx"
