#pragma once

# include "raytracer.hh"

namespace raytracer
{
  inline
  Raytracer::Raytracer(const scene::Scene* scene)
  : scene_(scene)
  , anti_aliasing_(1)
  , threads_(1)
  {
    // TODO: get the number of threads
    assert(threads_);
  }

  inline
  Raytracer::~Raytracer()
  {
    delete scene_;
  }

  inline
  const scene::Scene*
  Raytracer::scene_get() const
  {
    return scene_;
  }

  inline
  unsigned int
  Raytracer::anti_aliasing_get() const
  {
    return anti_aliasing_;
  }

  inline
  unsigned int
  Raytracer::threads_get() const
  {
    return threads_;
  }

  inline
  void
  Raytracer::anti_aliasing_set(unsigned int anti_aliasing)
  {
    anti_aliasing_ = anti_aliasing;
  }

  inline
  void
  Raytracer::threads_set(unsigned int threads)
  {
    threads_ = threads;
  }

  inline
  void
  Raytracer::verbose_set(bool verbose)
  {
    verbose_ = verbose;
  }

} // raytracer
