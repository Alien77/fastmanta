#pragma once

# include <string>

# include "scene/scene.hh"
# include "scene/camera.hh"
# include "scene/screen.hh"
# include "shapes/shape.hh"
# include "shapes/sphere.hh"
# include "shapes/plane.hh"
# include "shapes/torus.hh"
# include "shapes/csg-shape.hh"
# include "lights/light.hh"
# include "lights/ambient.hh"
# include "lights/directional.hh"
# include "lights/positional.hh"

namespace parser
{
  scene::Scene*
  get_scene_from(const std::string& file);

  const scene::Camera*
  parse_camera(std::ifstream& input);

  scene::Screen*
  parse_screen(std::ifstream& input);

  const shapes::Sphere*
  parse_sphere(std::ifstream& input);

  const shapes::Plane*
  parse_plane(std::ifstream& input);

  const shapes::Torus*
  parse_torus(std::ifstream& input);

  const shapes::CSGShape*
  parse_csg_shape(std::ifstream& input);

  misc::Color
  parse_ambient(std::ifstream& input);

  const lights::Directional*
  parse_directional(std::ifstream& input);

  const lights::Positional*
  parse_positional(std::ifstream& input);

} //parser
