#include <iostream>
#include <fstream>
#include <string>

#include "parser.hh"
#include "misc/color.hh"

namespace parser
{
  scene::Scene*
  get_scene_from(const std::string& file)
  {
    std::ifstream input(file);

    std::string current;

    std::getline(input, current);
    assert(current == "CAMERA");
    auto* camera = parse_camera(input);

    std::getline(input, current);
    std::getline(input, current);
    assert(current == "SCREEN");
    auto* screen = parse_screen(input);

    screen->center_set(camera);
    auto* scene = new scene::Scene(screen, camera);

    std::getline(input, current);
    std::getline(input, current);
    assert(current == "SHAPES");

    while (1)
    {
      std::getline(input, current);
      if (current == "LIGHTS")
        break;

      if (current == "SPHERE")
        scene->add_shape(parse_sphere(input));
      else if (current == "PLANE")
        scene->add_shape(parse_plane(input));
      else if (current == "TORUS")
        scene->add_shape(parse_torus(input));
      else if (current == "CSG")
        scene->add_shape(parse_csg_shape(input));
      else
        assert(false);

      std::getline(input, current);
    }

    auto ambient = misc::Color(0.0, 0.0, 0.0);

    while (1)
    {
      std::getline(input, current);
      if (current == "")
        break;

      if (current == "AMBIENT")
        ambient = ambient + parse_ambient(input);
      else if (current == "DIRECTIONAL")
        scene->add_light(parse_directional(input));
      else if (current == "POSITIONAL")
        scene->add_light(parse_positional(input));
      else
        assert(false);

      std::getline(input, current);
    }

    scene->ambient_set(new lights::Ambient(ambient));
    return scene;
  }

  const scene::Camera*
  parse_camera(std::ifstream& input)
  {
    double pos_x;
    double pos_y;
    double pos_z;

    input >> pos_x >> pos_y >> pos_z;

    double u_x;
    double u_y;
    double u_z;

    input >> u_x >> u_y >> u_z;

    double v_x;
    double v_y;
    double v_z;

    input >> v_x >> v_y >> v_z;

    return new scene::Camera(misc::Vector3(pos_x, pos_y, pos_z),
                             misc::Vector3(u_x, u_y, u_z),
                             misc::Vector3(v_x, v_y, v_z));
  }

  scene::Screen*
  parse_screen(std::ifstream& input)
  {
    unsigned int width;
    unsigned int height;

    input >> width >> height;

    return new scene::Screen(width, height);
  }

  template <std::string& Shape>
  const shapes::Shape*
  parse_shape(std::ifstream& input)
  {
    // unreachable
    assert(false);
  }

  const shapes::Sphere*
  parse_sphere(std::ifstream& input)
  {
    double radius;
    double pos_x;
    double pos_y;
    double pos_z;

    input >> radius >> pos_x >> pos_y >> pos_z;

    double r;
    double g;
    double b;

    input >> r >> g >> b;

    double diff;
    double spec;
    double shin;
    double refl;

    input >> diff >> spec >> shin >> refl;

    return new shapes::Sphere(shapes::Material(misc::Color(r, g, b), diff,
                              spec, shin, refl),
                              misc::Vector3(pos_x, pos_y, pos_z),
                              radius);
  }

  const shapes::Plane*
  parse_plane(std::ifstream& input)
  {
    double a;
    double b;
    double c;
    double d;

    input >> a >> b >> c >> d;

    double r;
    double g;
    double blue;

    input >> r >> g >> blue;

    double diff;
    double spec;
    double shin;
    double refl;

    input >> diff >> spec >> shin >> refl;

    misc::Vector3 normal(a, b, c);
    normal.normalize();

    return new shapes::Plane(shapes::Material(misc::Color(r, g, blue), diff,
                             spec, shin, refl), normal, d);
  }

  const shapes::Torus*
  parse_torus(std::ifstream& input)
  {
    double r;
    double R;
    double x;
    double y;
    double z;

    input >> r >> R >> x >> y >> z;

    double red;
    double g;
    double b;

    input >> red >> g >> b;

    double diff;
    double spec;
    double shin;
    double refl;

    input >> diff >> spec >> shin >> refl;

    return new shapes::Torus(shapes::Material(misc::Color(red, g, b), diff,
                             spec, shin, refl),
                             misc::Vector3(x, y, z),
                             r, R);
  }

  const shapes::CSGShape*
  parse_csg_shape(std::ifstream& input)
  {
    std::string current;
    std::getline(input, current);

    const shapes::Shape* left = nullptr;
    const shapes::Shape* right = nullptr;

    if (current == "SPHERE")
      left = parse_sphere(input);
    else if (current == "PLANE")
      left = parse_plane(input);
    else if (current == "TORUS")
      left = parse_torus(input);
    else if (current == "CSG")
      left = parse_csg_shape(input);
    else
      assert(false);

    std::getline(input, current);
    std::getline(input, current);

    if (current == "SPHERE")
      right = parse_sphere(input);
    else if (current == "PLANE")
      right = parse_plane(input);
    else if (current == "TORUS")
      right = parse_torus(input);
    else if (current == "CSG")
      right = parse_csg_shape(input);
    else
      assert(false);

    std::getline(input, current);
    std::getline(input, current);

    if (current != "UNION" && current != "INTER" && current != "DIFF")
      assert(false);

    double r;
    double g;
    double b;

    input >> r >> g >> b;

    double diff;
    double spec;
    double shin;
    double refl;

    input >> diff >> spec >> shin >> refl;


    return new shapes::CSGShape(shapes::Material(misc::Color(r, g, b), diff,
                                spec, shin, refl), left, right, current);
  }

  misc::Color
  parse_ambient(std::ifstream& input)
  {
    double r;
    double g;
    double b;

    input >> r >> g >> b;

    return misc::Color(r, g, b);
  }

  const lights::Directional*
  parse_directional(std::ifstream& input)
  {
    double dir_x;
    double dir_y;
    double dir_z;

    input >> dir_x >> dir_y >> dir_z;

    double r;
    double g;
    double b;

    input >> r >> g >> b;

    return new lights::Directional(misc::Color(r, g, b),
                                   misc::Vector3(dir_x, dir_y, dir_z));
  }

  const lights::Positional*
  parse_positional(std::ifstream& input)
  {
    double pos_x;
    double pos_y;
    double pos_z;

    input >> pos_x >> pos_y >> pos_z;

    double r;
    double g;
    double b;

    input >> r >> g >> b;

    return new lights::Positional(misc::Color(r, g, b),
                                   misc::Vector3(pos_x, pos_y, pos_z));
  }

} // parser
