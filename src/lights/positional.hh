#pragma once

# include "light.hh"
# include "ambient.hh"
# include "misc/vector.hh"

namespace lights
{
  class Positional: public Light
  {
    public:
      Positional(const misc::Color& color, const misc::Vector3& position);
      ~Positional();

      const misc::Ray*
      get_ray(const misc::Vector3& point) const override;

      const misc::Color apply_on(const shapes::Shape* shape,
                                 const misc::Vector3& normal,
                                 const misc::Vector3& point,
                                 const lights::Ambient* amb,
                                 const misc::Ray& ray) const override;

      double distance_from(const misc::Vector3& point) const override;

      const misc::Vector3& position_get() const;

    private:
      const misc::Vector3 position_;
  };

} // lights

# include "positional.hxx"
