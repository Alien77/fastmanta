#include <algorithm>
#include <cfloat>

#include "directional.hh"

namespace lights
{
  const misc::Color
  Directional::apply_on(const shapes::Shape* shape,
                        const misc::Vector3& normal,
                        const misc::Vector3& point,
                        const lights::Ambient* ambient,
                        const misc::Ray& ray) const
  {
    auto diffuse = color_  * ((-1 * dir_) | normal)
                   * shape->material_get().diff_get();
    auto specular = apply_specular(ray, -1 * dir_, point, normal,
                    shape->material_get().shin_get()) * color_
                    * shape->material_get().spec_get();

    return (diffuse + specular) * shape->material_get().color_get()
           * ambient->color_get();
  }

  const misc::Ray*
  Directional::get_ray(const misc::Vector3& point) const
  {
    return new misc::Ray(point, -1.0 * dir_);
  }

  double
  Directional::distance_from(const misc::Vector3&) const
  {
    return DBL_MAX;
  }

} // lights
