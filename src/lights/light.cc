#include <cmath>
#include <algorithm>

#include "light.hh"

namespace lights
{
  double
  Light::apply_specular(const misc::Ray& ray,
                        const misc::Vector3& init_dir,
                        const misc::Vector3& inte,
                        const misc::Vector3& normal,
                        double shininess) const
  {
    auto view = ray.origin_get() - inte;
    view.normalize();

    auto dot = 2.0 * (init_dir | normal);
    auto reflect = (dot * normal) - init_dir;
    reflect.normalize();

    return std::pow(std::max((view | reflect), 0.0), shininess);
  }

} // lights
