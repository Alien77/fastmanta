#pragma once

# include "directional.hh"

namespace lights
{
  inline
  Directional::Directional(const misc::Color& color, const misc::Vector3& dir)
  : Light(color)
  , dir_(dir)
  {}

  inline
  Directional::~Directional()
  {}

  inline
  const misc::Vector3&
  Directional::dir_get() const
  {
    return dir_;
  }

} // lights
