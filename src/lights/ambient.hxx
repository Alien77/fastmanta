#pragma once

# include "ambient.hh"
# include "misc/values.hh"

namespace lights
{
  inline
  Ambient::Ambient(const misc::Color& color)
  : Light(color)
  {}

  inline
  Ambient::~Ambient()
  {}

  inline
  const misc::Color
  Ambient::apply_on(const shapes::Shape* shape,
                    const misc::Vector3&,
                    const misc::Vector3&,
                    const lights::Ambient*,
                    const misc::Ray&) const
  {
    return color_ * shape->material_get().color_get() * values::AMBIENT_COEF_;
  }

  inline
  const misc::Ray*
  Ambient::get_ray(const misc::Vector3&) const
  {
    return nullptr;
  }

  inline
  double
  Ambient::distance_from(const misc::Vector3&) const
  {
    return 0.0;
  }

} // lights
