#include <algorithm>

#include "positional.hh"

namespace lights
{
  const misc::Color
  Positional::apply_on(const shapes::Shape* shape,
                       const misc::Vector3& normal,
                       const misc::Vector3& point,
                       const lights::Ambient* ambient,
                       const misc::Ray& ray) const
  {
    auto dist = misc::Vector3::distance_between(point, position_);
    auto dir = position_ - point;
    dir.normalize();

    auto diffuse = color_ * (dir | normal)
                   * shape->material_get().diff_get();
    auto specular = apply_specular(ray, dir, point, normal,
                    shape->material_get().shin_get()) * color_
                    * shape->material_get().spec_get();

    return (diffuse + specular) * shape->material_get().color_get()
           * ambient->color_get() / dist;
  }

  const misc::Ray*
  Positional::get_ray(const misc::Vector3& point) const
  {
    auto dir = position_ - point;
    dir.normalize();

    return new misc::Ray(point, dir);
  }

  double
  Positional::distance_from(const misc::Vector3& point) const
  {
    return misc::Vector3::distance_between(point, position_);
  }

} // lights
