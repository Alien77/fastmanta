#pragma once

# include "misc/color.hh"
# include "misc/vector.hh"
# include "misc/ray.hh"
# include "shapes/shape.hh"

namespace lights
{
  class Ambient;

  class Light
  {
    public:
      Light(const misc::Color& color);
      virtual ~Light();

      const misc::Color& color_get() const;

      double apply_specular(const misc::Ray& ray,
                            const misc::Vector3& init_dir,
                            const misc::Vector3& point,
                            const misc::Vector3& normal,
                            double shininess) const;

      virtual const misc::Ray*
      get_ray(const misc::Vector3& point) const = 0;

      virtual const misc::Color apply_on(const shapes::Shape* shape,
                                         const misc::Vector3& normal,
                                         const misc::Vector3& point,
                                         const lights::Ambient* amb,
                                         const misc::Ray& ray) const = 0;

      virtual double distance_from(const misc::Vector3& point) const = 0;

    protected:
      const misc::Color color_;
  };

} // lights

# include "light.hxx"
