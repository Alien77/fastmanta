#pragma once

# include "light.hh"
# include "misc/color.hh"

namespace lights
{
  class Ambient : public Light
  {
    public:
      Ambient(const misc::Color& color);
      ~Ambient();

      const misc::Color apply_on(const shapes::Shape* shape,
                                 const misc::Vector3& normal,
                                 const misc::Vector3& point,
                                 const lights::Ambient* amb,
                                 const misc::Ray& ray) const override;

      double distance_from(const misc::Vector3& point) const override;

      const misc::Ray*
      get_ray(const misc::Vector3& point) const override;
  };

} // lights

# include "ambient.hxx"
