#pragma once

# include "positional.hh"

namespace lights
{
  inline
  Positional::Positional(const misc::Color& color, const misc::Vector3& position)
  : Light(color)
  , position_(position)
  {}

  inline
  Positional::~Positional()
  {}

  inline
  const misc::Vector3&
  Positional::position_get() const
  {
    return position_;
  }

} // lights
