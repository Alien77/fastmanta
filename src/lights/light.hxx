#pragma once

# include "light.hh"

namespace lights
{
  inline
  Light::Light(const misc::Color& color)
  : color_(color)
  {}

  inline
  Light::~Light()
  {}

  inline
  const misc::Color&
  Light::color_get() const
  {
    return color_;
  }

} // lights
