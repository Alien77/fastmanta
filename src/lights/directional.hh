#pragma once

# include "light.hh"
# include "ambient.hh"
# include "misc/vector.hh"

namespace lights
{
  class Directional: public Light
  {
    public:
      Directional(const misc::Color& color, const misc::Vector3& dir);
      ~Directional();

      virtual const misc::Ray*
      get_ray(const misc::Vector3& point) const override;

      const misc::Color apply_on(const shapes::Shape* shape,
                                 const misc::Vector3& normal,
                                 const misc::Vector3& point,
                                 const lights::Ambient* amb,
                                 const misc::Ray& ray) const override;

      double distance_from(const misc::Vector3& point) const override;

      const misc::Vector3& dir_get() const;

    private:
      const misc::Vector3 dir_;
  };

} // lights

# include "directional.hxx"
