#pragma once

# include <vector>
# include <queue>
# include <memory>
# include <thread>
# include <mutex>
# include <condition_variable>
# include <future>
# include <functional>
# include <stdexcept>

namespace threads
{
  class ThreadPool final
  {
    public:
      ThreadPool(size_t);
      ~ThreadPool();

      template<class F, class... Args>
      auto enqueue(F&& f, Args&&... args)
      -> std::future<typename std::result_of<F(Args...)>::type>;

    private:
      // Need to keep track of threads so we can join them
      std::vector<std::thread> workers;

      // The task queue
      std::queue<std::function<void()>> tasks;

      // Synchronization
      std::mutex queue_mutex;
      std::condition_variable condition;
      bool stop;
  };

} // threads

# include "thread-pool.hxx"
