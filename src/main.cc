#include <iostream>

#include "boost/program_options.hpp"
#include "raytracer/raytracer.hh"
#include "shapes/sphere.hh"
#include "shapes/plane.hh"
#include "lights/ambient.hh"
#include "lights/directional.hh"
#include "lights/positional.hh"
#include "io/parser.hh"
#include "misc/values.hh"

int
main(int argc, char* argv[])
{
  namespace bpo = boost::program_options;
  bpo::options_description desc("Options");
  desc.add_options()
    ("help,h",                                      "Display some help")
    ("version,v",                                   "Version of FastManta")
    ("verbose",                                     "Verbose mode")
    ("threads,t",       bpo::value<unsigned int>(), "Number of threads used")
    ("anti-aliasing,a", bpo::value<unsigned int>(), "Specify anti-aliasing")
    ("input,i",         bpo::value<std::string>()->required(),  "Input file")
    ("output,o",        bpo::value<std::string>(),  "Output file");

  bpo::variables_map vm;
  try
  {
    bpo::store(bpo::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help"))
    {
      std::cout << desc << std::endl;
      return values::SUCCES_;
    }
    else if (vm.count("version"))
    {
      std::cout << "FastManta - Version v0.2 - @Alien" << std::endl
                << "Copyright 2016" << std::endl;
      return values::SUCCES_;
    }

    bpo::notify(vm);
  }
  catch(bpo::error& e)
  {
    std::cerr << "Error: " << e.what() << std::endl << std::endl
              <<  desc << std::endl;

    return values::CMD_ERROR_;
  }

  auto file = vm["input"].as<std::string>();

  if (vm.count("verbose"))
    std::cout << "Parsing scene from " << file << "... ";

  auto* scene = parser::get_scene_from(file);

  if (vm.count("verbose"))
    std::cout << "Done." << std::endl
              << "Initializing ray tracer... ";

  raytracer::Raytracer raytracer(scene);

  if (vm.count("verbose"))
  {
    raytracer.verbose_set(true);
    std::cout << "Done." << std::endl
              << "Applying options... ";
  }

  if (vm.count("anti-aliasing"))
    raytracer.anti_aliasing_set(vm["anti-aliasing"].as<unsigned int>());

  if (vm.count("verbose"))
    std::cout << "Done." << std::endl
              << "Starting ray tracing of the scene... " << std::endl;

  if (vm.count("output"))
    raytracer.raytrace_scene(vm["output"].as<std::string>());
  else
    raytracer.raytrace_scene("output");

  return values::SUCCES_;
}
