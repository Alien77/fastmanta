all: fastmanta

fastmanta:
	cd build/ && make fastmanta && mv fastmanta ..

clean:
	rm -rf fastmanta *.ppm

distclean: clean
	rm -rf build/

.PHONY: fastmanta clean distclean
